package com.bhahusut.emakneeb;

import java.util.ArrayList;

import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.color.Color;

import com.bhahusut.emakneeb.Data.BoardPoint;
import com.bhahusut.emakneeb.Data.makSprite;

public class PlayScene extends BaseScene {
	BitmapTextureAtlas play_bgT, play_boardT, play_wmakT, play_bmakT,
		play_vmakT,h_turnT,c_turnT;
	ITextureRegion play_bgTR, play_boardTR, play_wmakTR, play_bmakTR,
		play_vmakTR,h_turnTR,c_turnTR;
	BitmapTextureAtlas play1_bgT, play1_boardT, play1_wmakT, play1_bmakT,
		play1_vmakT;
	ITextureRegion play1_bgTR, play1_boardTR, play1_wmakTR, play1_bmakTR,
		play1_vmakTR;
	BitmapTextureAtlas play2_bgT, play2_boardT, play2_wmakT, play2_bmakT,
		play2_vmakT;
	ITextureRegion play2_bgTR, play2_boardTR, play2_wmakTR, play2_bmakTR,
		play2_vmakTR;
	public Sprite play_bgS,h_turnS,c_turnS;
	
	Text countNotEatT;
	
	RemoteData rd;
	
	Font textF;
	
	ArrayList<Sprite> vmoveSpriteAL = new ArrayList<Sprite>();
	
	Sound t1_eatS, t1_movedS, t2_eatS, t2_movedS, movedS, eatS;
	int tnum;
	String bdata;
	Text countNotEat;
	public int maxNotEat = 20;
	int notEatCount = 0;
	public int squareSize = 75;
	public int squareMargin = 14;
	int boardx = 340+60, boardy = 60+80;//add for flee ads banner
	char da[][];
	Sprite blS;
	PlayScene sc;
	
	ArrayList<makSprite> makSpriteAL = new ArrayList<makSprite>();
	
	public PlayScene(Engine _engine, VertexBufferObjectManager _vbo,
			BaseGameActivity _act,int _tnum) {
		super(_engine, _vbo, _act);
		tnum = _tnum;		
		sc = this;
	}

	@Override
	public void createScene() {											
		bdata = genStartBoard(false);
		da = splitData2Array(bdata);
		rd = new RemoteData(act);
	}
														
	@Override
	public void onBackKeyPressed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub
		play_boardT.unload();
		play_bgT.unload();
		play_wmakT.unload();
		play_bmakT.unload();
		play_vmakT.unload();

		eatS.release();
		movedS.release();				
	}		
	
	public void loadResource() {
		this.loadGraphics();
		this.loadFonts();
		this.loadSound();
		
		play_bgS = new Sprite(0,0,play_bgTR,vbo);
		blS = new Sprite(boardx, boardy, play_boardTR, vbo);
		h_turnS = new Sprite(100,330,h_turnTR,vbo);
		c_turnS = new Sprite(100,330,c_turnTR,vbo);
		
	}
	
	public void drawScene() {
		this.drawBackground();
		this.drawBoard();	
		
	}
	
	private void drawBackground()
	{				
		this.attachChild(play_bgS);
	}
	 
	void loadGraphics() {
		if (tnum == 1) {
			BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/theme1/");
		} else if (tnum == 2) {
			BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/theme2/");
		}
		
		h_turnT = new BitmapTextureAtlas(engine.getTextureManager(), 100, 171,
				TextureOptions.DEFAULT);
		h_turnTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				h_turnT, act, "human.png", 0, 0);
		h_turnT.load();
		
		
		c_turnT = new BitmapTextureAtlas(engine.getTextureManager(), 100, 108,
				TextureOptions.DEFAULT);
		c_turnTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				c_turnT, act, "computer.png", 0, 0);
		c_turnT.load();
		
		play_bgT = new BitmapTextureAtlas(engine.getTextureManager(), 1024, 768,
				TextureOptions.DEFAULT);
		play_bgTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				play_bgT, act, "bg.png", 0, 0);
		play_bgT.load();
		play_bmakT = new BitmapTextureAtlas(engine.getTextureManager(), 50, 50,
				TextureOptions.DEFAULT);
		play_bmakTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				play_bmakT, act, "bmak.png", 0, 0);
		play_bmakT.load();
		play_wmakT = new BitmapTextureAtlas(engine.getTextureManager(), 50, 50,
				TextureOptions.DEFAULT);
		play_wmakTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				play_wmakT, act, "wmak.png", 0, 0);
		play_wmakT.load();
		play_vmakT = new BitmapTextureAtlas(engine.getTextureManager(), 50, 50,
				TextureOptions.DEFAULT);
		play_vmakTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				play_vmakT, act, "vmak.png", 0, 0);
		play_vmakT.load();
		play_boardT = new BitmapTextureAtlas(engine.getTextureManager(), 600, 600,
				TextureOptions.DEFAULT);
		play_boardTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				play_boardT, act, "board.png", 0, 0);
		play_boardT.load();
						
	}
	 
	 void loadFonts() {
		 FontFactory.setAssetBasePath("font/");
         final ITexture fontTexture = new BitmapTextureAtlas(
					act.getTextureManager(), 1024, 1024,
					TextureOptions.BILINEAR_PREMULTIPLYALPHA);

			textF = FontFactory.createFromAsset(act.getFontManager(), fontTexture,
					act.getAssets(), "TAHOMA.TTF", 40, true,
					Color.WHITE.getARGBPackedInt());
			textF.load();

	 }
	 
	 void loadSound() {
		 if (tnum == 1) {
            SoundFactory.setAssetBasePath("mfx/theme1/");
		 } else if (tnum == 2) {
			 SoundFactory.setAssetBasePath("mfx/theme2/");
		 }

		 try {
				eatS = SoundFactory
						.createSoundFromAsset(engine.getSoundManager(),
								act.getApplicationContext(), "eat.mp3");
				movedS = SoundFactory.createSoundFromAsset(
						engine.getSoundManager(), act.getApplicationContext(),
						"moved.mp3");

		} catch (Exception e) {
			e.printStackTrace();
		}	 
	 }	 	 	 	 

	 public void drawBoard() {
			this.detachChildren();
			if (makSpriteAL != null)
				makSpriteAL.clear();

			this.drawBackground();

			this.attachChild(c_turnS);
			c_turnS.setVisible(false);
			
			this.attachChild(h_turnS);						
			
			// Text
			countNotEatT = new Text(40, 250, textF, "Turn left "
					+ (maxNotEat - notEatCount), vbo);			
			this.attachChild(blS);
			// draw text
			this.attachChild(countNotEatT);

			String bdata = joinData(da);
			drawBoardData(boardx, boardy, bdata);

	} 
	 
	public void drawCountNotEat() {
		countNotEatT.detachSelf();
		countNotEatT.dispose();
		
		countNotEatT = new Text(40, 250, textF, "Turn left "
				+ (maxNotEat - notEatCount), vbo);
		
		this.attachChild(countNotEatT);
	}
	 
    public void drawBoardData(int bx, int by, String data) {
		Sprite p1S, p2S;
		int ix = 0, iy = 0;
		
		for (int i = 0; i < 64; i++) {
			// Log.e("i",i+"");
			if (data.charAt(i) == '1') {
				int sx = ix * squareSize + squareMargin + bx;
				int sy = iy * squareSize + squareMargin + by;
				p1S = new Sprite(sx, sy, play_wmakTR, vbo);

				BoardPoint bp = new BoardPoint(ix, iy);
				makSprite ms = new makSprite(bp, p1S);
				makSpriteAL.add(ms);

				this.attachChild(p1S);
			} else if (data.charAt(i) == '2') {
				int sx = ix * squareSize + squareMargin + bx;
				int sy = iy * squareSize + squareMargin + by;
				p2S = new Sprite(sx, sy, play_bmakTR, vbo);

				BoardPoint bp = new BoardPoint(ix, iy);
				makSprite ms = new makSprite(bp, p2S);
				makSpriteAL.add(ms);

				this.attachChild(p2S);
			}

			ix++;
			if (ix > 7) {
				ix = 0;
				iy++;
			}
		}
	}
 		
	public void changeSpriteMakPos(int _fx,int _fy,int _tx,int _ty) {
		final int fx=_fx,fy=_fy,tx=_tx,ty=_ty;
		
		act.runOnUiThread(new Runnable() {
			@Override
	        public void run() {		
				BoardPoint bptmp = new BoardPoint(fx,fy);
				int z = Data.indexOfmakSpriteAL(makSpriteAL, bptmp);
				makSprite mSp = makSpriteAL.get(z);
				int posMarginX = squareMargin+boardx;
				int posMarginY = squareMargin+boardy;		
				int move_tx = tx*squareSize+posMarginX; 
				int move_ty = ty*squareSize+posMarginY;
				mSp.sp.setPosition(move_tx,move_ty);
			}
		});
	}
	
	public String joinData(char da[][]) {
		String data = "";
		for (int i=0;i<8;i++) {
			for (int j=0;j<8;j++) {
				data += da[j][i];
			}			
		}
		return data;
	}
	
	public void drawValidMove(ArrayList<BoardPoint> bpAL) {
		vmoveSpriteAL.clear();
		for (int i = 0; i < bpAL.size(); i++) {
			int x, y;
			x = bpAL.get(i).x;
			y = bpAL.get(i).y;
			// Log.e("xx",x+" "+y);
			Sprite vmS = new Sprite(x * squareSize + squareMargin + boardx, y
					* squareSize + squareMargin + boardy, play_vmakTR, vbo);
			this.attachChild(vmS);
			vmoveSpriteAL.add(vmS);
		}
	}
	
	public void hideValidMoveSprite() {
		for (Sprite sp : vmoveSpriteAL)
			sp.detachSelf();
	}
	
	public void removeMakSprite(makSprite _sp) {				
		final makSprite fms = _sp;
		
		engine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				sc.detachChild(fms.sp);
				fms.sp.dispose();
				
			}
		});
		
	}
	
	public void restartGame() {
		notEatCount = 0;
		bdata = genStartBoard(false);
		da = splitData2Array(bdata);
		drawBoard();
	}
	
	public String genStartBoard(boolean p1up) {
		String data;

		if (p1up) {
			data = "10101010" + "01010101";
			String datap2 = "00000000" + "22222222";
			data += "00000000" + "00000000" + "00000000" + "00000000";
			data += datap2;
		} else {
			data = "22222222" + "00000000";
			String datap2 = "00000000" + "11111111";
			data += "00000000" + "00000000" + "00000000" + "00000000";
			data += datap2;
		}

		return data;
	}

	public char[][] splitData2Array(String data) {
		char [][] da = new char[8][8];
		
		int ix=0,iy=0;
		for (int i=0;i<64;i++) {
			da[ix][iy] = data.charAt(i);
			ix++;
			if (ix > 7) {
				ix = 0;
				iy++;
			}
						
		}
		
		return da;		
	}
	
	public void setMak(int x,int y,char p) {
		da[x][y] = p;		
	}
	
	public void setMakSpriteBP(BoardPoint fbp,BoardPoint _tbp) {
		final BoardPoint tbp = new BoardPoint(_tbp.x,_tbp.y);
		for (int i=0;i<makSpriteAL.size();i++) {
			if (makSpriteAL.get(i).posEquals(fbp)) {
				final int j = i;
				engine.runOnUpdateThread(new Runnable() {
					@Override
					public void run() {
						makSpriteAL.get(j).bp.x = tbp.x;
						makSpriteAL.get(j).bp.y = tbp.y;						
					}					
				});
				
				break;
			}
		}
	}
	
}
