package com.bhahusut.emakneeb;

import org.andengine.engine.Engine;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.BaseGameActivity;

import android.app.Activity;

public abstract class BaseScene extends Scene {
	BitmapTextureAtlas bgT;
	ITextureRegion bgTR;
	Sprite bgS;
	
	protected Engine engine;
    protected Activity activity;
    protected VertexBufferObjectManager vbo;
    protected BaseGameActivity act;
    
    public BaseScene(Engine _engine,VertexBufferObjectManager _vbo,BaseGameActivity _act)
    {        
        this.engine = _engine;
        this.vbo = _vbo;
        this.act = _act;
        this.createScene();
       
    }
    
    public abstract void createScene();
    public abstract void onBackKeyPressed();
    public abstract void disposeScene();
    
}
