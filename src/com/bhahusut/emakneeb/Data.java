package com.bhahusut.emakneeb;

import java.util.ArrayList;

import org.andengine.entity.sprite.Sprite;

public class Data {
	
	public static class BoardPoint {
		int x;
		int y;
		
		public BoardPoint() {
			x = 0;
			y = 0;
		}
		
		public BoardPoint(int _x,int _y) {
			x = _x;
			y = _y;
		}
		
		public boolean equals(BoardPoint bp) {
			if (bp.x == x && bp.y == y)
				return true;
			else
				return false;
		}
	}
	
	public static class MoveBoardPoint {
		int fx,tx;
		int fy,ty;
				
		public MoveBoardPoint() {
			tx = 0;
			fx = 0;
			fy = 0;
			ty = 0;
		}
	}
		
	public static class SumScore {
		int val;
		MoveBoardPoint mbp;
		
		public SumScore() {
			val = 0;
			mbp = new MoveBoardPoint();
		}
		
		public void setMovePoint(int fx,int fy,int tx,int ty) {
			mbp.fx = fx;
			mbp.tx = tx;
			mbp.fy = fy;
			mbp.ty = ty;
		}
	}
	
	public static class makSprite {
		public BoardPoint bp;
		public Sprite sp;
		
		makSprite(BoardPoint _bp,Sprite _sp) {
			bp = _bp;
			sp = _sp;
		}
		
		public boolean posEquals(BoardPoint tmpbp) {
			if (tmpbp.x == bp.x && tmpbp.y == bp.y)
				return true;
			else
				return false;
		}
	}
	
	public static boolean isInBPAL(ArrayList<BoardPoint> bpal,
			BoardPoint bp) {
		for (int i=0;i<bpal.size();i++) {
			if (bpal.get(i).equals(bp)) {
				return true;
			}
		}
		
		return false;
	}
	
	public static int indexOfmakSpriteAL(ArrayList<makSprite> msal,
			BoardPoint bp) {		
		for (int i=0;i<msal.size();i++) {
			if (msal.get(i).posEquals(bp)) {
				return i;
			}
		}
		
		return -1;
	}
}
