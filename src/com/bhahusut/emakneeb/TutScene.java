package com.bhahusut.emakneeb;

import org.andengine.engine.Engine;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.BaseGameActivity;

public class TutScene extends BaseScene {

	BitmapTextureAtlas tutBackButtT,tutNextButtT,tut1T,tut2T
			,tut3T,tut4T;
	ITextureRegion tutBackButtTR,tutNextButtTR,tut1TR
			,tut2TR,tut3TR,tut4TR;
	Sprite bgS,backButtS,nextButtS,tut1S,tut2S,tut3S,tut4S;
	
	public TutScene(Engine _engine, VertexBufferObjectManager _vbo,
			BaseGameActivity _act) {
		super(_engine, _vbo, _act);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createScene() {
		// TODO Auto-generated method stub
		this.loadGraphics();
		this.createBackground();
		this.createSprites();
	}

	@Override
	public void onBackKeyPressed() {		
		
	}

	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub
		
	}
	
	private void createBackground()
	{		
		bgS = new Sprite(0,0,bgTR,vbo);
		this.attachChild(bgS);
		
	}
	 
	private void createSprites() {
		backButtS = new Sprite(50,658,tutBackButtTR,vbo);
		nextButtS = new Sprite(800,658,tutNextButtTR,vbo);
		tut1S = new Sprite(252,124,tut1TR,vbo);
		tut2S = new Sprite(252,124,tut2TR,vbo);
		tut3S = new Sprite(252,124,tut3TR,vbo);
		tut4S = new Sprite(252,124,tut4TR,vbo);
		
	}
	
	void loadGraphics() {						
		 BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/tut/");
			bgT = new BitmapTextureAtlas(engine.getTextureManager(), 
					1024,768, TextureOptions.DEFAULT);
			bgTR = BitmapTextureAtlasTextureRegionFactory.
					createFromAsset(bgT, act, "bg.png", 0, 0);
			bgT.load();			
			
			tut1T = new BitmapTextureAtlas(engine.getTextureManager(), 
					520,520, TextureOptions.DEFAULT);
			tut1TR = BitmapTextureAtlasTextureRegionFactory.
					createFromAsset(tut1T, act, "tut1.png", 0, 0);
			tut1T.load();
			
			tut2T = new BitmapTextureAtlas(engine.getTextureManager(), 
					520,520, TextureOptions.DEFAULT);
			tut2TR = BitmapTextureAtlasTextureRegionFactory.
					createFromAsset(tut2T, act, "tut2.png", 0, 0);
			tut2T.load();
			
			tut3T = new BitmapTextureAtlas(engine.getTextureManager(), 
					520,520, TextureOptions.DEFAULT);
			tut3TR = BitmapTextureAtlasTextureRegionFactory.
					createFromAsset(tut3T, act, "tut3.png", 0, 0);
			tut3T.load();
			
			tut4T = new BitmapTextureAtlas(engine.getTextureManager(), 
					520,520, TextureOptions.DEFAULT);
			tut4TR = BitmapTextureAtlasTextureRegionFactory.
					createFromAsset(tut4T, act, "tut4.png", 0, 0);
			tut4T.load();
			
			tutBackButtT = new BitmapTextureAtlas(engine.getTextureManager(), 
					185,104, TextureOptions.DEFAULT);
			tutBackButtTR = BitmapTextureAtlasTextureRegionFactory.
					createFromAsset(tutBackButtT, act, "back_butt.png", 0, 0);
			tutBackButtT.load();
			
			tutNextButtT = new BitmapTextureAtlas(engine.getTextureManager(), 
					188,104, TextureOptions.DEFAULT);
			tutNextButtTR = BitmapTextureAtlasTextureRegionFactory.
					createFromAsset(tutNextButtT, act, "next_butt.png", 0, 0);
			tutNextButtT.load();
	}	 	
	
	public void drawTut(int num) {
		this.detachChildren();
		if (num == 1) {
			this.attachChild(bgS);
			this.attachChild(nextButtS);
			this.attachChild(tut1S);
		} else if (num == 2) {
			this.attachChild(bgS);
			this.attachChild(nextButtS);
			this.attachChild(backButtS);
			this.attachChild(tut2S);
		} else if (num == 3) {
			this.attachChild(bgS);
			this.attachChild(nextButtS);
			this.attachChild(backButtS);
			this.attachChild(tut3S);
		} else if (num == 4) {
			this.attachChild(bgS);				
			this.attachChild(backButtS);
			this.attachChild(tut4S);
		}
	}
	
}
