package com.bhahusut.emakneeb;

import java.util.ArrayList;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.opengl.view.RenderSurfaceView;
import org.andengine.ui.activity.BaseGameActivity;

import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.widget.FrameLayout;

import com.bhahusut.emakneeb.Data.BoardPoint;
import com.bhahusut.emakneeb.Data.MoveBoardPoint;
import com.bhahusut.emakneeb.Data.makSprite;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class MainActivity extends BaseGameActivity {

	final int mCameraWidth = 1024;
	final int mCameraHeight = 768;
	public ResultScene res_scene;
	public LevelScene level_scene;
	public TutScene tut_scene;
	public TitleScene title_scene;
	public LocationScene loc_scene;
	public PlayScene play_scene;
	public WaitScene wait_scene;
	
	int tutSceneNum = 1;
	int themeNum = 1;
	MainActivity act;
	int maxNotEat = 20;
	
	
	public boolean isEndGame = false;

	BitmapTextureAtlas resLostBGT, resWinBGT;
	ITextureRegion resLostBGTR, resWinBGTR;

	Sprite resWinS, resLostS;
	
	Text countNotEatT, countEndT;
	int touchState = 0;
	public VertexBufferObjectManager vbo;
	RemoteData rd;
	ArrayList<BoardPoint> vmoveAL;
	boolean isShowValidMove = false;
	int oldix, oldiy;
	char oldcmak;
	
	char pSide = '1';
	char playerMak = '1';
	MoveBoardPoint nbp;
	int ix, iy;
	char cmak;
	Engine eg;
	makSprite mSp;
	boolean makStopMoving = false;
	ArrayList<BoardPoint> ateBP;
	boolean countEndState = false;
	
	int pWin = '1';
	boolean drawBoardFinished = false;

	final static int title_scene_num = 0;
	final static int tut_scene_num = 1;
	final static int wait_scene_num = 2;
	final static int result_scene_num = 3;
	final static int play_scene_num = 4;
	final static int level_scene_num = 5;
	final static int location_scene_num = 6;
		
	int scene_num = 0;
	
	public void setScene(int num) {
		switch (num) {
		case title_scene_num :
			mEngine.setScene(title_scene);
			scene_num = title_scene_num;
			break;
		case tut_scene_num :
			mEngine.setScene(tut_scene);
			scene_num = tut_scene_num;
			break;
		case wait_scene_num :
			mEngine.setScene(wait_scene);
			scene_num = wait_scene_num;
			break;
		case result_scene_num :
			mEngine.setScene(res_scene);
			scene_num = result_scene_num;
			break;
		case play_scene_num :
			mEngine.setScene(play_scene);
			play_scene.drawCountNotEat();
			scene_num = play_scene_num;
			break;
		case level_scene_num :
			mEngine.setScene(level_scene);
			scene_num = level_scene_num;
			break;
		case location_scene_num :
			mEngine.setScene(loc_scene);
			scene_num = location_scene_num;
			break;
		}
	}
	
	@Override
    protected void onSetContentView() {
		final FrameLayout frameLayout = new FrameLayout(this);
		final FrameLayout.LayoutParams frameLayoutParams = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT, Gravity.FILL);
		final FrameLayout.LayoutParams adViewLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.LEFT | Gravity.TOP);
		
		final AdView adView = new AdView(this);
		adView.setAdUnitId("ca-app-pub-4080668725279052/5873932781");
		adView.setAdSize(AdSize.BANNER);
        adView.setVisibility(AdView.VISIBLE);
        adView.refreshDrawableState();
		
        /*
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
        		.addTestDevice("B3D229A5E2D9D063F6DF28ED85B03D1E")
        		.build();*/
        AdRequest adRequest = new AdRequest.Builder().build();
        
        adView.loadAd(adRequest);        
		
		this.mRenderSurfaceView = new RenderSurfaceView(this);
		mRenderSurfaceView.setRenderer(mEngine,this);
		
		final FrameLayout.LayoutParams surfaceViewLayoutParams = new FrameLayout.LayoutParams(
				android.view.ViewGroup.LayoutParams.MATCH_PARENT,
				android.view.ViewGroup.LayoutParams.MATCH_PARENT);
		surfaceViewLayoutParams.gravity = Gravity.LEFT;
		
		frameLayout.addView(this.mRenderSurfaceView,surfaceViewLayoutParams);
		frameLayout.addView(adView,adViewLayoutParams);
		this.setContentView(frameLayout,frameLayoutParams);		
		
	}
	
	@Override
	public EngineOptions onCreateEngineOptions() {
		Camera mCamera = new Camera(0, 0, mCameraWidth, mCameraHeight);

		final EngineOptions engineOptions = new EngineOptions(true,
				ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(
						mCameraWidth, mCameraHeight), mCamera);
		engineOptions.getAudioOptions().setNeedsSound(true);
		engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
		return engineOptions;

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{  
	    if (keyCode == KeyEvent.KEYCODE_BACK)
	    {
	        switch (scene_num) {
	        case title_scene_num :
	        	System.exit(0);
	        	break;
	        case level_scene_num :
	        	setScene(title_scene_num);
	        	break;
	        case tut_scene_num :
	        	setScene(title_scene_num);
	        	break;
	        case location_scene_num :
	        	setScene(title_scene_num);
	        	break;
	        case play_scene_num :
	        	pSide = '1';
	        	play_scene.restartGame();	        	
	        	setScene(title_scene_num);
	        	break;
	        }
	    }
	    return false; 
	}
	
	@Override
	public void onCreateResources(
			OnCreateResourcesCallback pOnCreateResourcesCallback)
			throws Exception {
		rd = new RemoteData(act);
		act = this;

		loadGraphics();
		loadSounds();
		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
			throws Exception {
		Log.e("start", "create scene");

		vbo = mEngine.getVertexBufferObjectManager();

		//play_scene = new PlayScene(mEngine, vbo, this,1);		

		level_scene = new LevelScene(mEngine, vbo, this);
		createOnTouchLevelScene();

		tut_scene = new TutScene(mEngine, vbo, this);
		createOnTouchTutScene();

		loc_scene = new LocationScene(mEngine, vbo, this);
		createOnTouchLocScene();

		res_scene = new ResultScene(mEngine, vbo, this);
		createOnTouchResultScene();

		title_scene = new TitleScene(mEngine, vbo, this);
		createOnTouchTitleScene();

		wait_scene = new WaitScene(mEngine, vbo, this);
		
		pOnCreateSceneCallback.onCreateSceneFinished(title_scene);

	}

	void createOnTouchResultScene() {
		res_scene.setOnSceneTouchListener(new IOnSceneTouchListener() {
			@Override
			public boolean onSceneTouchEvent(Scene pScene,
					TouchEvent pSceneTouchEvent) {
				if (pSceneTouchEvent.isActionUp()) {
					int ix = (int) pSceneTouchEvent.getX();
					int iy = (int) pSceneTouchEvent.getY();
					
					if (ix >= 90+200 && ix <= 200+200 && iy >= 295+150 && iy <= 407+150) { // play again
						pSide = '1';
						play_scene.restartGame();						
						setScene(play_scene_num);						
					} else if (ix >= 419+200 && ix <= 525+200 && iy >= 295+150 && iy <= 407+150) {// return to main menu
						pSide = '1';
						play_scene.restartGame();						
						setScene(title_scene_num);
					}
				}								
				return false;
			}
			
		});
		
	}
	
	void createOnTouchTitleScene() {
		title_scene.setOnSceneTouchListener(new IOnSceneTouchListener() {
			@Override
			public boolean onSceneTouchEvent(Scene pScene,
					TouchEvent pSceneTouchEvent) {
				if (pSceneTouchEvent.isActionUp()) {
					int ix = (int) pSceneTouchEvent.getX();
					int iy = (int) pSceneTouchEvent.getY();

					if (ix >= 341 && ix <= 703 && iy >= 277 && iy <= 379) { // play
						setScene(location_scene_num);
					} else if (ix >= 238 && ix <= 793 && iy >= 400 && iy <= 507) { // level
						setScene(level_scene_num);
					} else if (ix >= 298 && ix <= 748 && iy >= 523 && iy <= 636) { // tutorial
						setScene(tut_scene_num);
					}
				}
				return false;
			}
		});
	}

	void createResScene(char whoWin) {
		res_scene.detachChildren();

		resWinS = new Sprite(0, 0, resWinBGTR, vbo);
		resLostS = new Sprite(0, 0, resLostBGTR, vbo);

		if (whoWin == playerMak) {
			res_scene.attachChild(resWinS);
		} else {
			res_scene.attachChild(resLostS);
		}

		res_scene.setOnSceneTouchListener(new IOnSceneTouchListener() {
			@Override
			public boolean onSceneTouchEvent(Scene pScene,
					TouchEvent pSceneTouchEvent) {
				if (pSceneTouchEvent.isActionUp()) {
					int ix = (int) pSceneTouchEvent.getX();
					int iy = (int) pSceneTouchEvent.getY();

					if (ix >= 85 && ix <= 200 && iy >= 296 && iy <= 400) {// retry
						res_scene.detachSelf();
						pSide = '1';
						play_scene.restartGame();						
					} else if (ix >= 418 && ix <= 527 && iy >= 296 && iy <= 400) {// return
																					// to
																					// menu
						setScene(title_scene_num);
					}
				}

				return false;
			}
		});
	}

	void createOnTouchLocScene() {		
		loc_scene.setOnSceneTouchListener(new IOnSceneTouchListener() {
			@Override
			public boolean onSceneTouchEvent(Scene pScene,
					TouchEvent pSceneTouchEvent) {
				if (pSceneTouchEvent.isActionUp()) {
					int ix = (int) pSceneTouchEvent.getX();
					int iy = (int) pSceneTouchEvent.getY();

					if (ix >= 142 && ix <= 455 && iy >= 278 && iy <= 561) {// left						
						themeNum = 2;
						setScene(wait_scene_num);
						play_scene = new PlayScene(mEngine,vbo,act,themeNum);
						play_scene.maxNotEat = maxNotEat;
						play_scene.loadResource();
						play_scene.drawScene();
						createOnTouchPlayScene();
						setScene(play_scene_num);												
					} else if (ix >= 568 && ix <= 904 && iy >= 278 && iy <= 561) {// right
						themeNum = 1;
						setScene(wait_scene_num);
						play_scene = new PlayScene(mEngine,vbo,act,themeNum);
						play_scene.maxNotEat = maxNotEat;
						play_scene.loadResource();
						play_scene.drawScene();
						createOnTouchPlayScene();
						setScene(play_scene_num);
						
					}
				}

				return false;
			}
		});
	}

	void createOnTouchTutScene() {

		tut_scene.drawTut(1);
		tut_scene.setOnSceneTouchListener(new IOnSceneTouchListener() {
			@Override
			public boolean onSceneTouchEvent(Scene pScene,
					TouchEvent pSceneTouchEvent) {
				if (pSceneTouchEvent.isActionUp()) {
					int ix = (int) pSceneTouchEvent.getX();
					int iy = (int) pSceneTouchEvent.getY();

					if (tutSceneNum == 1) {
						if (ix >= 800 && ix <= 800 + 188 && iy >= 658
								&& iy <= 658 + 104) {// next butt
							tut_scene.drawTut(2);
							tutSceneNum = 2;
						}
					} else if (tutSceneNum == 2) {
						if (ix >= 50 && ix <= 50 + 185 && iy >= 658
								&& iy <= 658 + 104) {// back butt
							tut_scene.drawTut(1);
							tutSceneNum = 1;
						} else if (ix >= 800 && ix <= 800 + 188 && iy >= 658
								&& iy <= 658 + 104) {// next butt
							tut_scene.drawTut(3);
							tutSceneNum = 3;
						}
					} else if (tutSceneNum == 3) {
						if (ix >= 50 && ix <= 50 + 185 && iy >= 658
								&& iy <= 658 + 104) {// back butt
							tut_scene.drawTut(2);
							tutSceneNum = 2;
						} else if (ix >= 800 && ix <= 800 + 188 && iy >= 658
								&& iy <= 658 + 104) {// next butt
							tut_scene.drawTut(4);
							tutSceneNum = 4;
						}
					} else if (tutSceneNum == 4) {
						if (ix >= 50 && ix <= 50 + 185 && iy >= 658
								&& iy <= 658 + 104) {// back butt
							tut_scene.drawTut(3);
							tutSceneNum = 3;
						}
					}
				}
				return false;
			}
		});
	}

	void createOnTouchLevelScene() {
		level_scene.setOnSceneTouchListener(new IOnSceneTouchListener() {
			@Override
			public boolean onSceneTouchEvent(Scene pScene,
					TouchEvent pSceneTouchEvent) {
				if (pSceneTouchEvent.isActionUp()) {
					int ix = (int) pSceneTouchEvent.getX();
					int iy = (int) pSceneTouchEvent.getY();

					if (ix >= 327 && ix <= 651 && iy >= 169 && iy <= 292) {
						// touch easy button
						rd.maxDepth = 1;
						maxNotEat = 20;
						setScene(title_scene_num);
					} else if (ix >= 335 && ix <= 652 && iy >= 339 && iy <= 442) {
						rd.maxDepth = 2;
						maxNotEat = 40;
						setScene(title_scene_num);
					}

				}

				// Log.e("maxDepth",rd.maxDepth+"");

				return false;
			}
		});
	}

	void processPlayResult() {
		int comMakCount = 0, playerMakCount = 0;
		if (playerMak == '1') {
			comMakCount = rd.countMak(play_scene.da, '2');
		} else {
			playerMakCount = rd.countMak(play_scene.da, '1');
		}

		if (playerMakCount > comMakCount) {

			play_scene.attachChild(res_scene);
		}
	}

	void createOnTouchPlayScene() {	
		pSide = '1';
		// touching		
		play_scene.setOnSceneTouchListener(new IOnSceneTouchListener() {
			@Override
			public boolean onSceneTouchEvent(Scene pScene,
					TouchEvent pSceneTouchEvent) {				
				
				if (pSceneTouchEvent.isActionUp()) {
					act.disableTouching();
					
					float tx = pSceneTouchEvent.getX();
					float ty = pSceneTouchEvent.getY();
					ix = (int) (tx - play_scene.boardx) / play_scene.squareSize;
					iy = (int) (ty - play_scene.boardy) / play_scene.squareSize;

					// if touch x,y not in board position return
					if (!rd.isInsideBoard(ix, iy)) {
						return false;
					}

					cmak = rd.getBoardMak(play_scene.da, ix, iy);

					if (isShowValidMove) {
						// check if touch valid move
						vmoveAL = rd.findAllValidMove(play_scene.da, oldix, oldiy);
						// check if no validMove
						if (vmoveAL.size() == 0) {
							endGame();
							return false;
						}
						// move mak
						BoardPoint tmpbp = new BoardPoint();
						tmpbp.x = ix;
						tmpbp.y = iy;
						if (Data.isInBPAL(vmoveAL, tmpbp)) {
														
							// player side
							play_scene.setMak(oldix, oldiy, '0');
							play_scene.setMak(ix, iy, playerMak);														
							//set makSprite
							BoardPoint fbp = new BoardPoint(oldix,oldiy);
							BoardPoint tbp = new BoardPoint(ix,iy);							
							play_scene.setMakSpriteBP(fbp, tbp);
							
							
							//rd.dumpBoard("dump", play_scene.da);
							
							BoardPoint bptmp = new BoardPoint(oldix,oldiy);
							 
							int z = Data.indexOfmakSpriteAL(play_scene.makSpriteAL, bptmp);
							makSprite mSp = play_scene.makSpriteAL.get(z);
							int posMarginX = play_scene.squareMargin+play_scene.boardx;
							int posMarginY = play_scene.squareMargin+play_scene.boardy;
							int move_fx = oldix*play_scene.squareSize+posMarginX;
							int move_tx = ix*play_scene.squareSize+posMarginX;
							int move_fy = oldiy*play_scene.squareSize+posMarginY; 
							int move_ty = iy*play_scene.squareSize+posMarginY;		  
						
							int count = rd.countMovingLength(oldix,oldiy 
									 ,ix, iy);
							float dur = (float) (count*0.2);
							
							play_scene.hideValidMoveSprite();
							
							MoveModifier mm1 = new
									 MoveModifier(dur, move_fx,move_tx,move_fy,move_ty ) {
								 		@Override 
								 		protected void onModifierFinished(IEntity pItem) { //chk
								 			//set mak appearance														
											play_scene.movedS.play();
											isShowValidMove = false;																																															
											// check player eat
											//Log.e("chkEat",ix+" "+iy);
											ateBP = new ArrayList<BoardPoint>();
											ateBP = rd.chkEat(play_scene.da, ix, iy, playerMak);
											if (ateBP.size() == 0) {
												play_scene.notEatCount++;
												play_scene.drawCountNotEat();
											} else {
												for (int i = 0; i < ateBP.size(); i++) {
													int x = ateBP.get(i).x;
													int y = ateBP.get(i).y;
													
													play_scene.setMak(x, y, '0');													
													
													for (int j=0;j<play_scene.makSpriteAL.size();j++) {
														if (play_scene.makSpriteAL.get(j).bp.x == x &&
															play_scene.makSpriteAL.get(j).bp.y == y) {
															play_scene.makSpriteAL.get(j).sp.setVisible(false);
															play_scene.removeMakSprite(play_scene.makSpriteAL.get(j));
															play_scene.makSpriteAL.remove(j);
														}																											
													}																																																
												}
												
												play_scene.eatS.play();											
												play_scene.notEatCount = 0;
											}
								
											if (play_scene.notEatCount > play_scene.maxNotEat) {
												endGame();
											}

											if (rd.isGameOver(play_scene.da) != -1) {
												endGame();
																								
											}																							
											
											//computer side												
											pSide = '2';
											updateSide();
											
											play_scene.registerUpdateHandler(new TimerHandler(0.5f, true, new ITimerCallback() {
									            @Override
									            public void onTimePassed(final TimerHandler pTimerHandler) {					            						            	
									            	nbp = rd.chooseBestMove(play_scene.da, '2');
									            	
									            	// move computer side																																										
													if (nbp.fx == nbp.tx && nbp.fy == nbp.ty) {
														endGame(); 														
													}
													  													
											        play_scene.setMak(nbp.fx, nbp.fy, '0');
											        play_scene.setMak(nbp.tx, nbp.ty, '2');											        											        											        
											        //set makSprite point
													BoardPoint fbp = new BoardPoint(nbp.fx,nbp.fy);
													BoardPoint tbp = new BoardPoint(nbp.tx,nbp.ty);							
													play_scene.setMakSpriteBP(fbp, tbp);
											        											        											        
											        
											        //animate move BoardPoint 
													BoardPoint bptmp = new BoardPoint(nbp.fx,nbp.fy);
														 
													int z = Data.indexOfmakSpriteAL(play_scene.makSpriteAL, bptmp);
													makSprite mSp = play_scene.makSpriteAL.get(z);
													int posMarginX = play_scene.squareMargin+play_scene.boardx;
													int posMarginY = play_scene.squareMargin+play_scene.boardy;
													int move_fx = nbp.fx*play_scene.squareSize+posMarginX;
													int move_tx = nbp.tx*play_scene.squareSize+posMarginX;
													int move_fy = nbp.fy*play_scene.squareSize+posMarginY; 
													int move_ty = nbp.ty*play_scene.squareSize+posMarginY;		  
												
													int count = rd.countMovingLength(mSp.bp.x,mSp.bp.y 
															 , nbp.tx, nbp.ty);
													float dur = (float) (count*0.2);		  													 													
													
													MoveModifier mm2 = new
														 MoveModifier(dur, move_fx,move_tx,move_fy,move_ty ) {
													 		@Override 
													 		protected void onModifierFinished(IEntity pItem) { //chk eat 													 																 																 			
													 			play_scene.registerUpdateHandler(new TimerHandler(0.2f, true, new ITimerCallback() {
														            @Override
														            public void onTimePassed(final TimerHandler pTimerHandler) {
														            	ateBP = rd.chkEat(play_scene.da,nbp.tx, nbp.ty, '2');					  				  													 																 			
													 			
																		if (ateBP.size() == 0) {//no ate 
																			  play_scene.notEatCount++;
																			  play_scene.drawCountNotEat();
																			  play_scene.movedS.play();
																		} else {//clear ate mak					  												   
																			for (int i=0;i<ateBP.size();i++) {
																				int x = ateBP.get(i).x; 
																				int y = ateBP.get(i).y;																		
																				play_scene.setMak(x, y, '0');
																				
																				for (int j=0;j<play_scene.makSpriteAL.size();j++) {
																					if (play_scene.makSpriteAL.get(j).bp.x == x &&
																						play_scene.makSpriteAL.get(j).bp.y == y) {
																						play_scene.makSpriteAL.get(j).sp.setVisible(false);
																						play_scene.removeMakSprite(play_scene.makSpriteAL.get(j));
																						play_scene.makSpriteAL.remove(j);
																					}																											
																				}																																																						
																			}
																			play_scene.eatS.play();
																			play_scene.notEatCount = 0;																			
																		}
																			  												  
																		if (play_scene.notEatCount > play_scene.maxNotEat) { 
																			endGame();
																		}
																		   
																		//play_scene.drawBoard();																																				
																		  
																		if (rd.isGameOver(play_scene.da) != -1) { //restart game
																			endGame();
																		    makStopMoving = true;
																		}			
																		
																		pSide = '1';
																		updateSide();
																		
																		play_scene.unregisterUpdateHandler(pTimerHandler);
														            }
														         }));
														 	} 
							
													};
							     					mSp.sp.registerEntityModifier(mm2);							     									 									            	
									            	play_scene.unregisterUpdateHandler(pTimerHandler);
									            	
									            }
									        }));											
								 		}								 		
							};
							
							mSp.sp.registerEntityModifier(mm1);																					
							play_scene.clearEntityModifiers();
														
							
						} else {
							// touch other point
							isShowValidMove = false;
							//play_scene.drawBoard();
							//hide validmove
							play_scene.hideValidMoveSprite();
							
						}

					} else { // if not shown validMove

						// touch player mak
						ArrayList<BoardPoint> maktmp = new ArrayList<BoardPoint>();
						BoardPoint tmpbp = new BoardPoint();

						maktmp = rd.findAllMak(play_scene.da, playerMak);

						tmpbp = new BoardPoint();
						tmpbp.x = ix;
						tmpbp.y = iy;
						if (Data.isInBPAL(maktmp, tmpbp)) {
							// touch player mak
							vmoveAL = rd.findAllValidMove(play_scene.da, ix, iy);
							play_scene.drawBoard();
							play_scene.drawValidMove(vmoveAL);

							isShowValidMove = true;
						}
					}

					// save old touch position/mak
					oldix = ix;
					oldiy = iy;
					oldcmak = cmak;
				}
				
				play_scene.setOnSceneTouchListener(this);

				return true;
			}
		});

	}

	public void endGame() {
		int p1count = rd.countMak(play_scene.da,'1');
		int p2count = rd.countMak(play_scene.da,'2');
		
		int status = 0;
		if (p1count > p2count) {//win
			status = 1;
		} else if (p1count < p2count) {//lost
			status = -1;
		} else {//draw
			status = 0;
		}		
					
		this.createOnTouchPlayScene();
		isEndGame = false;
		
		res_scene.drawScene(status);
		setScene(result_scene_num);				
	}
	
	@Override
	public void onPopulateScene(Scene pScene,
			OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {

		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}

	private void loadGraphics() {						
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/result/");
		resLostBGT = new BitmapTextureAtlas(getTextureManager(), 625, 465,
				TextureOptions.DEFAULT);
		resLostBGTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				resLostBGT, this, "lost.png", 0, 0);
		resLostBGT.load();

		resWinBGT = new BitmapTextureAtlas(getTextureManager(), 625, 465,
				TextureOptions.DEFAULT);
		resWinBGTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				resWinBGT, this, "win.png", 0, 0);
		resWinBGT.load();

	}
	
	private void loadSounds() {
		
	}	

	public void updateSide() {				
		if (pSide == '1') {
			play_scene.h_turnS.setVisible(true);
			play_scene.c_turnS.setVisible(false);
		} else if (pSide == '2') {
			play_scene.h_turnS.setVisible(false);
			play_scene.c_turnS.setVisible(true);
		}		
		
		//Log.e("xx","pSide "+pSide);		
	}

	public void disableTouching() {
		play_scene.setOnSceneTouchListener(new IOnSceneTouchListener() {
			@Override
			public boolean onSceneTouchEvent(Scene pScene,
					TouchEvent pSceneTouchEvent) {
				
					return false;
			}
		});
	}
	
	
}
