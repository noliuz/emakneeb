package com.bhahusut.emakneeb;

import java.util.ArrayList;
import java.util.Random;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.BaseGameActivity;

import android.util.Log;

import com.bhahusut.emakneeb.Data.BoardPoint;
import com.bhahusut.emakneeb.Data.MoveBoardPoint;
import com.bhahusut.emakneeb.Data.SumScore;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class RemoteData {

	char da[][];
	public String ret="";
	public int maxDepth = 1,nodeCount=0;
	int cur_mak_x,cur_mak_y,cur_move_x,cur_move_y,sumScore;
	Scene publicScene;
	boolean waitedScene = false;
	ArrayList<SumScore> sumScoreAL;
	int move_x=0,move_y=0;

	SumScore ss;
	ArrayList<Thread> threadAL;
	public int countEnd = 0;
	BaseGameActivity act;
	
	public RemoteData(BaseGameActivity _act) {
		act = _act;
	}
	
	public String getBoardData(String objId) {
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Board");
				
		/*query.findInBackground(new FindCallback<ParseObject>() {
		    public void done(List<ParseObject> data, ParseException e) {		        
		    	if (e == null) {
		            ret =  data.get(0).getString("data");
		        } else {
		            Log.e("getBoardData", "Error: " + e.getMessage());
		        }
		    }
		});*/
		ret = "zzz";
		ParseObject obj = null;
		try {
			obj = query.get(objId);
		} catch (Exception e) {
			Log.e("error",e.getMessage());
		}
			
		return obj.getString("data");
	}
	
	
	
	public boolean validMove(String data,int makx,int maky) {
		char da[][];
		boolean valid = true;
		
		da = splitData2Array(data);
		
		ArrayList<Integer> vxAL = new ArrayList<Integer>();
		ArrayList<Integer> vyAL = new ArrayList<Integer>();
		
		//find all valid move
			//check inside board move
		if (makx >= 0 && makx <= 7 && maky >= 0 && maky <= 7)
			valid = true;
		else
			return false;
			//
			
		
		return true;		
	}
	
	public ArrayList<BoardPoint> findAllValidMove(char da[][],
			int makx,int maky) {
		
		ArrayList<BoardPoint> bpAL = new ArrayList<BoardPoint>();
		
		//check up
		int j=maky;
		while (true) {
			j--;
			if (isMakEmpty(da,makx,j)) {
				BoardPoint bp = new BoardPoint();
				bp.x = makx;
				bp.y = j;
				bpAL.add(bp);
				
			} else {
				break;
			}				
		}
		
		//check down
		j=maky;
		while (true) {
			j++;
			
			if (isMakEmpty(da,makx,j)) {
				BoardPoint bp = new BoardPoint();
				bp.x = makx;
				bp.y = j;
				bpAL.add(bp);
			} else {
				break;
			}				
		}
	
		//check left
		int i=makx;
		while (true) {
			i--;
			if (isMakEmpty(da,i,maky)) {
				BoardPoint bp = new BoardPoint();
				bp.x = i;
				bp.y = maky;
				bpAL.add(bp);
			} else {
				break;
			}				
		}
		
		//check right
		i=makx;
		while (true) {
			i++;
			if (isMakEmpty(da,i,maky)) {
				BoardPoint bp = new BoardPoint();
				bp.x = i;
				bp.y = maky;
				bpAL.add(bp);
			} else {
				break;
			}				
		}
		
		return bpAL;		
	}
	public boolean isMakEmpty(char da[][],int makx,int maky) {
		
		if (!isInsideBoard(makx,maky))
			return false;
		
		if (da[makx][maky] == '0') {
			//Log.e("xx",makx+" "+maky);
			return true;
		} else {
			return false;
		}
		
	}
	
	public int countMovingLength(int fx,int fy,int tx,int ty) {
		if (fx == tx) {
			return Math.abs(fy-ty);
		} else if (fy == ty) {
			return Math.abs(fx-tx);
		}
		
		return 0;		
	}
	
	public boolean isInsideBoard(int x,int y) {
		if (x >= 0 && x <= 7 && y >= 0 && y <= 7)
			return true;
		else 
			return false;
	}
	
	
	
	public String joinData(char da[][]) {
		String data = "";
		for (int i=0;i<8;i++) {
			for (int j=0;j<8;j++) {
				data += da[j][i];
			}			
		}
		return data;
	}
	
	public char getBoardMak(char da[][],int x,int y) {		
		return da[x][y];
	}
	
	public ArrayList<BoardPoint> chkPierceEat(char da[][],int mx,int my,char p) {
		ArrayList<BoardPoint> ateBP = new ArrayList<BoardPoint>();
		
		//check piercing center *x* ��ҡ�ҧ
		//horizontal
		char mp = '0';
		if (p == '1')
			mp = '2';
		else if (p == '2')
			mp = '1';
		
		ArrayList<Integer> cleftI = new ArrayList<Integer>();;
		if (this.isInsideBoard(mx-1, my)) {
			if (da[mx-1][my] == mp) {
				cleftI.add(-1);
			}
		}
				
		ArrayList<Integer> crightI = new ArrayList<Integer>();; 
		if (this.isInsideBoard(mx+1, my)) {
			if (da[mx+1][my] == mp) {
				crightI.add(1);
			}
		}		
		
		//Log.e("mm",crightI.size()+" "+cleftI.size());
		
		if (crightI.size() != 0 && cleftI.size() != 0) {
			//get right pos edge				
				BoardPoint tmpBP = new BoardPoint();
				tmpBP.x = mx+1;
				tmpBP.y = my;				
				ateBP.add(tmpBP);
			//get left pos edge				
				tmpBP = new BoardPoint();
				tmpBP.x = mx-1;
				tmpBP.y = my;				
				ateBP.add(tmpBP);
				
		}
			//vertical
				//down
		ArrayList<Integer> cdownI = new ArrayList<Integer>();
		if (this.isInsideBoard(mx, my+1)) {
			if (da[mx][my+1] == mp) {
				cdownI.add(1);
			}
		}
			//up
		ArrayList<Integer> cupI = new ArrayList<Integer>();;
		if (this.isInsideBoard(mx, my-1)) {
			if (da[mx][my-1] == mp) {
				cupI.add(-1);
			}
		}
				
		if (cdownI.size() != 0 && cupI.size() != 0) {
			//get up pos edge				
				BoardPoint tmpBP = new BoardPoint();
				tmpBP.x = mx;
				tmpBP.y = my-1;				
				ateBP.add(tmpBP);
			//get down pos edge				
				tmpBP = new BoardPoint();
				tmpBP.x = mx;
				tmpBP.y = my+1;				
				ateBP.add(tmpBP);				
		}
	
		return ateBP;
	}
	
	public ArrayList<BoardPoint> chkEat(char da[][],int mx,int my,char p) {
		//chkEat at position after moving mak
		// return ate mak point							
		
		ArrayList<BoardPoint> ateBP = new ArrayList<BoardPoint>();
								
		//check nip ˹պ		
			//check right band
		ArrayList<Integer> countI = 
				this.chkBand('r', da, mx, my, p,false);
		for (int i=0;i<countI.size();i++) {
			BoardPoint tmpBP = new BoardPoint();
			tmpBP.x = countI.get(i)+mx;
			tmpBP.y = my;
			ateBP.add(tmpBP);
		}
			//check left band
		ArrayList<Integer> countI2 = 
				this.chkBand('l', da, mx, my, p,false);
		for (int i=0;i<countI2.size();i++) {
			BoardPoint tmpBP = new BoardPoint();
			tmpBP.x = countI2.get(i)+mx;
			tmpBP.y = my;
			ateBP.add(tmpBP);
		}
			//check up band
		ArrayList<Integer> countI3 = 
				this.chkBand('u', da, mx, my, p,false);
		for (int i=0;i<countI3.size();i++) {
			BoardPoint tmpBP = new BoardPoint();
			tmpBP.y = countI3.get(i)+my;
			tmpBP.x = mx;
			ateBP.add(tmpBP);
		}
			//check down band
		ArrayList<Integer> countI4 = 
				this.chkBand('d', da, mx, my, p,false);
		for (int i=0;i<countI4.size();i++) {
			BoardPoint tmpBP = new BoardPoint();
			tmpBP.y = countI4.get(i)+my;
			tmpBP.x = mx;
			ateBP.add(tmpBP);
		}

		//if there are nip , do not check pierce				
		ArrayList<BoardPoint> ateBP1 = this.chkPierceEat(da, mx, my, p);
		
		ArrayList<BoardPoint> rAteBP = null;
		if (ateBP.size() >= ateBP1.size())		
			rAteBP = ateBP;
		else 
			rAteBP = ateBP1;
		
		return rAteBP;
	}
	
	public ArrayList<Integer> chkBand(char direction,char da[][]
			,int mx,int my,char p,boolean inverse) {
		/* 
		 * chkBand check to direction to found same mak //for nip
		 *   --inverse-- check to direction to found different mak // for 
		 *                                               pierce
		 * 'r' chk right band
		 * 'l' chk left band
		 * 'u' chk up band
		 * 'd' chk down band
		 * return -1 -2 -3 ... or 1 2 3 and 0 is checked position 
		 */
		ArrayList<Integer> posI = new ArrayList<Integer>();
		char mp = '0';
		if (p == '1')
			mp = '2';
		else if (p == '2')
			mp = '1';
		
		
		if (da[mx][my] != p)
			return posI;		
		
		boolean foundP = false;
		
		//chk if valid band
		if (direction == 'r') {			
			int count = 1;			
			while (isInsideBoard(mx+count,my)) {				
				if (da[mx+count][my] == '0') {
					break;
				}
								
				if (da[mx+count][my] == mp) { 
					posI.add(count);									
				} else if (da[mx+count][my] == p) {
					foundP = true;
					break;
				}
				
				count++;
			}
		
		} else if (direction == 'l') {
			int count = -1;			
			while (isInsideBoard(mx+count,my)) {				
				if (da[mx+count][my] == '0') {		
					break;
				}								
								
				if (da[mx+count][my] == mp) { 
					posI.add(count);									
				} else if (da[mx+count][my] == p) {
					foundP = true;
					break;
				}
				
				count--;
			}

		} else if (direction == 'u') {
			int count = -1;			
			while (isInsideBoard(mx,my+count)) {				
				if (da[mx][my+count] == '0') {
					break;
				}								
								
				if (da[mx][my+count] == mp) { 
					posI.add(count);								
				} else if (da[mx][my+count] == p) {
					foundP = true;
					break;
				}
				
				count--;
			}
		} else if (direction == 'd') {
			int count = 1;			
			while (isInsideBoard(mx,my+count)) {				
				if (da[mx][my+count] == '0') {
					break;
				}								
								
				if (da[mx][my+count] == mp) { 
					posI.add(count);									
				} else if (da[mx][my+count] == p) {
					foundP = true;
					break;
				}
				count++;
			}
		}
		
		if (!foundP)
			posI.clear();
		
		return posI;
	}
	
	public int getEdgePosBand(char direction,char da[][]
			,int mx,int my,char p) {
		/* find same mak as p
		 * 'r' chk right band
		 * 'l' chk left band
		 * 'u' chk up band
		 * 'd' chk down band
		 * return a index (... ,-1,0,1, ...) in axis 
		 */							
		boolean foundP = false;
		
		//chk if valid band
		if (direction == 'r') {			
			int count = 1;			
			while (isInsideBoard(mx+count,my)) {				
				if (da[mx+count][my] == '0') {
					foundP = false;
					break;
				}
				
				if (p == '1') {
					if (da[mx+count][my] == '1') { 
						foundP = true;
						return count;											
					}
					
				} else if (p == '2') {
					if (da[mx+count][my] == '2') { 
						foundP = true;
						return count;						
					}					
				}
				count++;
			}
		} else if (direction == 'l') {
			int count = -1;			
			while (isInsideBoard(mx+count,my)) {				
				if (da[mx+count][my] == '0') {
					foundP = false;
					break;
				}
					
				if (p == '1') {
					if (da[mx+count][my] == '1') { 
						foundP = true;
						return count;						
					}					
				} else if (p == '2') {
					if (da[mx+count][my] == '2') { 
						foundP = true;
						return count;		
					}					
				}
				count--;
			}

		} else if (direction == 'u') {
			int count = -1;			
			while (isInsideBoard(mx,my+count)) {				
				if (da[mx][my+count] == '0') {
					foundP = false;
					break;
				}
				
				if (p == '1') {
					if (da[mx][my+count] == '1') { 
						foundP = true;
						return count;						
					}
					
				} else if (p == '2') {
					if (da[mx][my+count] == '2') { 
						foundP = true;
						return count;						
					}					
				}
				count--;
			}
		} else if (direction == 'd') {
			int count = 1;			
			while (isInsideBoard(mx,my+count)) {				
				if (da[mx][my+count] == '0') {
					foundP = false;
					break;
				}
				
				if (p == '1') {
					if (da[mx][my+count] == '1') { 
						foundP = true;
						return count;
					}					
				} else if (p == '2') {
					if (da[mx][my+count] == '2') { 
						foundP = true;
						return count;						
					}					
				}
				count++;
			}
		}				
		
		return 0;
	}

	public int isGameOver(char da[][]) {
		/*return
		 * 2 win
		 * 1 win
		 * 0 draw
		 * -1 not yet
		 */
		int count1 = 0,count2 = 0;
		int treturn = 0;
		
		//all mak ate
		for (int i=0;i<8;i++) {
			for (int j=0;j<8;j++) {
				if (da[i][j] == '1')
					count1++;
				else if (da[i][j] == '2') 
					count2++;
			}
		}				
		
		if (count1 == 0 || count2 == 0) {
			if (count1 == 0)
				return 2;
			else if (count2 == 0)
				return 1;
		}						
	
		//win by �����訹
		/*
		if (countEnd == maxCountEnd) {	
			if (count1 == count2)
				return 0;
			else if (count1 > count2)
				return 1;
			else 
				return 2;						
		}*/		
		
		return -1;
	}
	
	public int minimaxFor1(char _da[][],int mx,int my,int depth,SumScore sumscore) {				
		nodeCount++;				
		
		char da[][] = new char[8][8];		
		dup_da(_da,da);
		
		int score;	 	
		//evaluate score '2' moving			
		ArrayList<BoardPoint> ate_bpAL = 
				this.chkEat(da, mx, my, '2');
		int eval_score = ate_bpAL.size();
		
		score = eval_score;	
		sumscore.val += score;
		
		
		if (eval_score != 0) {
			//dumpBoard("2eat board",da);
			//Log.e("score up1", score		+""	);
		}
		
		
		//clear ate for next depth
		for (int s=0;s<ate_bpAL.size();s++) {
			int x = ate_bpAL.get(s).x;
			int y = ate_bpAL.get(s).y;
			da[x][y] = '0';
		}
		
		if (isGameOver(da) != -1) {
			return score;			
		}				
		
		//Log.e("m1","1");
		
		int bestscore = 8;
		
		ArrayList<BoardPoint> mak_bpAL = findAllMak(da,'1');						
		
		for (int i=0;i<mak_bpAL.size();i++) {
			int mak_x = mak_bpAL.get(i).x;
			int mak_y = mak_bpAL.get(i).y;
				
			//Log.e("mak",mak_x+" "+mak_y);
			
			ArrayList<BoardPoint> move_bpAL = 
					findAllValidMove(da,mak_x,mak_y);
		
			for (int k=0;k<move_bpAL.size();k++) {
				int move_x = move_bpAL.get(k).x;
				int move_y = move_bpAL.get(k).y;
												
				//moving
				da[mak_x][mak_y] = '0';
				da[move_x][move_y] = '1';								
				
				//dumpBoard("for1",da);
				
				if (depth < maxDepth) {
					score = minimaxFor2(da,move_x,move_y,depth+1,sumscore);
				}																				
				
				if (bestscore > score) {
					bestscore = score;	
					//Log.e("best score2",bestscore+" "+move_x+" "+move_y);									
				}
				
				//return move
				da[mak_x][mak_y] = '1';
				da[move_x][move_y] = '0';
				
				//Log.e("score2",score+"");
			}															
		}								
		return bestscore;
	}

	public int minimaxFor2(char _da[][],int mx,int my,int depth,SumScore sumscore) {				
		nodeCount++;
		
		char da[][] = new char[8][8];		
		dup_da(_da,da);				
		
		int score;
		//evaluate score '1' moving
		ArrayList<BoardPoint> ate_bpAL = 
				this.chkEat(da, mx, my, '1');
		int eval_score = ate_bpAL.size();
		
		score = eval_score;
		sumscore.val -= score;
		
		
		if (eval_score != 0) {
			//dumpBoard("1eat board",da);
			//Log.e("score up2",score+"");
		}
		
		
		//clear ate for next depth
		for (int s=0;s<ate_bpAL.size();s++) {
			int x = ate_bpAL.get(s).x;
			int y = ate_bpAL.get(s).y;
			da[x][y] = '0';
		}
		
		if (isGameOver(da) != -1) {
			return score;			
		}				
		
		//Log.e("m2","2");
		
		int bestscore = 0;
		
		ArrayList<BoardPoint> mak_bpAL = findAllMak(da,'2');						
		
		/*
		mak_bpAL.clear();
		BoardPoint bp = new BoardPoint();
		bp.x = 3;
		bp.y = 7;
		mak_bpAL.add(bp);
		*/
		
		for (int i=0;i<mak_bpAL.size();i++) {
			int mak_x = mak_bpAL.get(i).x;
			int mak_y = mak_bpAL.get(i).y;
				
			//Log.e("mak",mak_x+" "+mak_y);
			
			ArrayList<BoardPoint> move_bpAL = 
					findAllValidMove(da,mak_x,mak_y);
		
			move_bpAL.size();
			
			for (int k=0;k<move_bpAL.size();k++) {
				int move_x = move_bpAL.get(k).x;
				int move_y = move_bpAL.get(k).y;
												
				//moving
				da[mak_x][mak_y] = '0';
				da[move_x][move_y] = '2';								
																
				//dumpBoard("for2",da);
				
				if (depth < maxDepth) {
					//Log.e("xx","xx");
					score = minimaxFor1(da,move_x,move_y,depth+1,sumscore);					
				}																				
				
				if (score > bestscore) {
					bestscore = score;	
					//Log.e("best score2",bestscore+" "+move_x+" "+move_y);									
				}
				
				//return move
				da[mak_x][mak_y] = '2';
				da[move_x][move_y] = '0';
				
				//Log.e("score2",score+"");
			}															
		}								
		return bestscore;
	}
	
	
	public MoveBoardPoint chooseBestMove(char _da[][],char _p) {		
		final char p = _p;
		da = new char[8][8];
		dup_da(_da,da);
				
		/*act.runOnUiThread(new Runnable() {		
			@Override
	        public void run() {*/										
				MoveBoardPoint nMoveBP = new MoveBoardPoint();		
				sumScoreAL = new ArrayList<SumScore>();		
				threadAL = new ArrayList<Thread>();
				
				int tscore,bestSumScore=-999999;				
						
				//find position of maks
				ArrayList<BoardPoint> mak_bpAL;
				mak_bpAL = findAllMak(da,p);		
					
				
				//AI computing				
				for (int i=0;i<mak_bpAL.size();i++) {// Loop mak
					int mak_x = mak_bpAL.get(i).x;
					int mak_y = mak_bpAL.get(i).y;
					
					//possible move positions
					ArrayList<BoardPoint> move_bpAL = findAllValidMove(da
							,mak_x,mak_y);						
					for (int k=0; k<move_bpAL.size();k++) { // Loop moving
						move_x = move_bpAL.get(k).x;
						move_y = move_bpAL.get(k).y;
									
						da[move_x][move_y] = p;
						da[mak_x][mak_y] = '0';
						nodeCount = 0;	
						ss = new SumScore();
						ss.setMovePoint(mak_x, mak_y,move_x, move_y);
						sumScoreAL.add(ss);
						
						if (p == '2') {
							Thread t = new Thread() {
								@Override
								public void run() {
									minimaxFor1(da,move_x,move_y,1,ss);							
								}												
							};	
							threadAL.add(t);
							t.start();															
						} else { 
							tscore = minimaxFor2(da,move_x,move_y,1,ss);
						}								
						
						//check all thread died
						while (true) {
							int count=0;
							for (int a=0;a<threadAL.size();a++) {
								Thread t = threadAL.get(a);
								if (!t.isAlive())
									count++;
							}
							if (count == threadAL.size())
								break;
						}
						
						//Log.e("sumscore_size"+k,sumScoreAL.size()+"");
						
						for (int l=0;l<sumScoreAL.size();l++) {
							SumScore ss1 = sumScoreAL.get(l);
							
							if (ss1.val > bestSumScore) {
								nMoveBP.fx = ss1.mbp.fx;
								nMoveBP.fy = ss1.mbp.fy;
								nMoveBP.tx = ss1.mbp.tx;
								nMoveBP.ty = ss1.mbp.ty;
								bestSumScore = ss1.val;
							} else if (ss1.val == bestSumScore) {
								Random r = new Random();
								int ran = r.nextInt(1-0+1)+0;
								if (ran == 1) {
									nMoveBP.fx = ss1.mbp.fx;
									nMoveBP.fy = ss1.mbp.fy;
									nMoveBP.tx = ss1.mbp.tx;
									nMoveBP.ty = ss1.mbp.ty;
								}
							}
						}
											
						da[move_x][move_y] = '0';
						da[mak_x][mak_y] = p;
						
					}					
				}
	/*		
			}
		});*/
		
		return nMoveBP;
	}	
	
	public ArrayList<BoardPoint> findAllMak(char da[][],char p) {
		ArrayList<BoardPoint> bpAL = new ArrayList<BoardPoint>();
		for (int i=0;i<8;i++) {
			for (int j=0;j<8;j++) {				
				if (da[i][j] == p) {
					BoardPoint bp = new BoardPoint();
					bp.x = i;
					bp.y = j;
					bpAL.add(bp);
				}
			}
		}
		
		return bpAL;
	}

	public void dumpBoard(String tag,char da[][]) {
		String data = "";
		for (int i=0;i<8;i++) {
			for (int j=0;j<8;j++) {
				data += da[j][i];
			}
			data += "\n";
		}
		
		Log.e(tag,data);
	}
	
	public void dup_da(char da[][],char to_da[][]) {
		for (int i=0;i<8;i++) {
			for (int j=0;j<8;j++) {
				to_da[i][j] = da[i][j];
			}
		}
	}
	
	public void sceneWait(Scene _sc,float ms) {		
		publicScene = _sc;		
		
		TimerHandler tm = new TimerHandler(ms,true,new ITimerCallback(){
	        @Override
	        public void onTimePassed(TimerHandler pTimerHandler) {
	        	waitedScene = true;
	        	publicScene.unregisterUpdateHandler(pTimerHandler);
	        }
        });
        publicScene.registerUpdateHandler(tm);
        
	}
	
	public int countMak(char da[][],char p) {
		int count=0;
		for (int i=0;i<8;i++) {
			for (int j=0;j<8;j++) {
				if (da[i][j] == p)
					count++;
			}
		}
		
		return count;
	}
	
	public char[][] splitData2Array(String data) {
		char [][] da = new char[8][8];
		
		int ix=0,iy=0;
		for (int i=0;i<64;i++) {
			da[ix][iy] = data.charAt(i);
			ix++;
			if (ix > 7) {
				ix = 0;
				iy++;
			}
						
		}
		
		return da;		
	}
	
}


