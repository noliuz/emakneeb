package com.bhahusut.emakneeb;

import org.andengine.engine.Engine;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.BaseGameActivity;

public class LocationScene extends BaseScene {

	public LocationScene(Engine _engine, VertexBufferObjectManager _vbo,
			BaseGameActivity _act) {
		super(_engine, _vbo, _act);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createScene() {
		// TODO Auto-generated method stub
		this.loadGraphics();
		createBackground(); 	
	}

	@Override
	public void onBackKeyPressed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub
		
	}

	private void createBackground()
	{		
		bgS = new Sprite(0,0,bgTR,vbo);
		this.attachChild(bgS);
	}
	 
	void loadGraphics() {
		 BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/location/");						
		 bgT = new BitmapTextureAtlas(engine.getTextureManager(), 
					1024, 768, TextureOptions.DEFAULT);			
		 bgTR = BitmapTextureAtlasTextureRegionFactory.
					createFromAsset(bgT, act, "main.png", 0, 0);
		 bgT.load();
	}
	 
	 void loadFonts() {
		 
	 }
	 
	 void loadSound() {
		 
	 }
}
