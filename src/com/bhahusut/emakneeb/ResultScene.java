package com.bhahusut.emakneeb;

import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.BaseGameActivity;

public class ResultScene extends BaseScene {

	ResultScene sc;
	BitmapTextureAtlas win_bgT,lost_bgT,draw_bgT;
	ITextureRegion win_bgTR,lost_bgTR,draw_bgTR;
	Sound winS,lostS,drawS;
	Sprite bgSP;
	
	public ResultScene(Engine _engine, VertexBufferObjectManager _vbo,
			BaseGameActivity _act) {
		super(_engine, _vbo, _act);

		sc = this;
	}

	@Override
	public void createScene() {
		// TODO Auto-generated method stub
		loadResource();						
	}

	@Override
	public void onBackKeyPressed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub
		
	}
	
	public void loadResource() {
		this.loadGraphics();	
		this.loadSound();
				
	}

	public void drawScene(int status) {
		/*
		 * -1 lost
		 * 0 draw
		 * 1 win
		 */
		
		Sound pS = null;
		
		switch (status) {
		case -1 :
			bgSP = new Sprite(200,150,lost_bgTR,vbo);
			pS = lostS;
			break;
		case 0 :
			bgSP = new Sprite(200,150,draw_bgTR,vbo);
			pS = drawS;
			break;
		case 1 :
			bgSP = new Sprite(200,150,win_bgTR,vbo);
			pS = winS;
			break;			
		}
		
		sc.attachChild(bgSP);
		pS.play();
		
	}
	
	public void loadGraphics() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/result/");
		win_bgT = new BitmapTextureAtlas(engine.getTextureManager(), 625, 465,
				TextureOptions.DEFAULT);
		win_bgTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				win_bgT, act, "win.png", 0, 0);
		win_bgT.load();
		
		draw_bgT = new BitmapTextureAtlas(engine.getTextureManager(), 625, 465,
				TextureOptions.DEFAULT);
		draw_bgTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				draw_bgT, act, "draw.png", 0, 0);
		draw_bgT.load();
		
		lost_bgT = new BitmapTextureAtlas(engine.getTextureManager(), 625, 465,
				TextureOptions.DEFAULT);
		lost_bgTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				lost_bgT, act, "lost.png", 0, 0);
		lost_bgT.load();
	}
	
	public void loadSound() {
		SoundFactory.setAssetBasePath("mfx/result/");
		try {
			winS = SoundFactory
					.createSoundFromAsset(engine.getSoundManager(),
							act.getApplicationContext(), "win.wav");
			drawS = SoundFactory.createSoundFromAsset(
					engine.getSoundManager(), act.getApplicationContext(),
					"draw.wav");
			lostS = SoundFactory.createSoundFromAsset(
					engine.getSoundManager(), act.getApplicationContext(),
					"lost.mp3");
			
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
	}
}
