package com.bhahusut.emakneeb;

import org.andengine.engine.Engine;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.BaseGameActivity;


public class TitleScene extends BaseScene {
	 
	public TitleScene(Engine _engine, VertexBufferObjectManager _vbo,
			BaseGameActivity _act) {
		super(_engine, _vbo, _act);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onBackKeyPressed() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub
		
	}

	@Override	
	public void createScene() {		 
		this.loadGraphics();
		createBackground(); 			
	}	
	 
	private void createBackground()
	{		
		bgS = new Sprite(0,0,bgTR,vbo);
		this.attachChild(bgS);
	}
	 
	void loadGraphics() {
		 BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/title/");						
		 bgT = new BitmapTextureAtlas(engine.getTextureManager(), 
					1024, 768, TextureOptions.DEFAULT);			
		 bgTR = BitmapTextureAtlasTextureRegionFactory.
					createFromAsset(bgT, act, "main.png", 0, 0);
		 bgT.load();
	}
	 
	 void loadFonts() {
		 
	 }
	 
	 void loadSound() {
		 
	 }

}
